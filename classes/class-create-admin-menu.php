<?php
/*
    This file will create admin menu page
*/ 

class SC_Create_admin_Page {
    public function __construct() {
        add_action('admin_menu', [$this, 'create_admin_menu']);
    }

    public function create_admin_menu() {
        add_menu_page(
            'Course Overview', // Title of the page
            'Course Overview', // Text to show on the menu link
            'manage_options', // Capability requirement to see the link
            'sc-settings', // The 'slug' - file to display when clicking the link
            [$this, 'menu_page_template'], // The function to be called to output the content for this page
            'dashicons-media-document'
        );
    }

    public function menu_page_template() {
        echo '<div class="wrap"><div id="sc-admin-app"></div></div>';
    }
}

new SC_Create_admin_Page();