<?php
/*
    This file will create admin menu page
*/ 

class SC_Create_footer_Page {
    public function __construct() {
        add_action( 'wp_footer', [$this, 'sho_courses_block'] );
    }

    public function sho_courses_block() {
        if(is_front_page()){
            echo '<div class="wrap"><div id="sc-footer-app"></div></div>';
        }
    }
}

new SC_Create_footer_Page();


