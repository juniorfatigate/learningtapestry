<?php 

class SC_Settings_Rest_Route {
    public function __construct() {
        add_action( 'rest_api_init', [ $this, 'create_rest_routes' ] );
    }

    public function create_rest_routes() {
        register_rest_route( 'sc/v1', '/settings', [
            'methods' => 'GET',
            'callback' => [ $this, 'get_settings' ]
        ] );
        register_rest_route( 'sc/v1', '/settings', [
            'methods' => 'POST',
            'callback' => [ $this, 'save_settings' ],
            'permission_callback' => [ $this, 'save_settings_permission' ]
        ] );
        register_rest_route( 'lt/v1', '/courses', [
            'methods' => 'GET',
            'callback' => [ $this, 'get_courses' ],
        ] );
    }

    public function get_settings() {
        $title = get_option("sc_settings_title");
        $description = get_option("sc_settings_description");
        $number_courses_display = get_option("sc_settings_number_courses_display");

        $response = [
            "title" => $title,
            "description" => $description,
            "number_courses_display" => $number_courses_display,
        ];

        return rest_ensure_response($response);
    }

    public function save_settings($request) {
        $title = sanitize_text_field($request['title']);
        $description = sanitize_text_field($request['description']);
        $number_courses_display = sanitize_text_field($request['number_courses_display']);

        update_option('sc_settings_title', $title);
        update_option('sc_settings_description', $description);
        update_option('sc_settings_number_courses_display', $number_courses_display);

        return rest_ensure_response('Success');
    }

    public function save_settings_permission() {
        return current_user_can('publish_posts');
    }

    public function get_courses($request) {
        $page = $request->get_param('page');
        $number_courses_display = get_option("sc_settings_number_courses_display");
        $response = [];

        $args = array(
            'posts_per_page' => $number_courses_display,
            'post_type' => 'courses',
            'post_status' => 'publish',
            'paged' => $page
        );
        
        $courses = get_posts( $args );

        foreach ($courses as $key => $course) {
            array_push($response, [
                "title" => get_the_title($course->ID),
                "excerpt" => wp_strip_all_tags( get_the_excerpt($course->ID), true ),
                "link" => get_permalink($course->ID)
            ]);
        }

        return rest_ensure_response($response);
    }
}

new SC_Settings_Rest_Route();