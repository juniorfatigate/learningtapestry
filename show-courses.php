<?php 
/*
Plugin Name: Show courses
Description: This plugin is responsible for displaying courses on the home page.
Author: Carlos Fatigate
Version: 1.0.0
Text-Domain: show-courses
*/

if( ! defined( 'ABSPATH' ) ) : exit(); endif; // No direct access allowed.

/**
* Define Plugins Contants
*/
define ( 'SC_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define ( 'SC_URL', trailingslashit( plugins_url( '/', __FILE__ ) ) );

/**
 * Loading Necessary Scripts
 */
add_action( 'admin_enqueue_scripts', 'load_scripts' );

function load_scripts() {
    wp_enqueue_script( 'show-courses', SC_URL . 'dist/bundle.js', [ 'jquery', 'wp-element' ], wp_rand(), true );
    wp_localize_script( 'show-courses', 'appLocalizer', [
        'apiUrl' => home_url( '/wp-json' ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );
}

require_once SC_PATH . 'classes/class-create-admin-menu.php';
require_once SC_PATH . 'classes/class-create-settings-routes.php';
require_once SC_PATH . 'classes/class-create-footer-test.php';

add_action( 'init', 'load_scripts_block' );

function load_scripts_block() {
    wp_enqueue_script( 'show-courses', SC_URL . 'dist/bundle.js', [ 'jquery', 'wp-element' ], wp_rand(), true );
    wp_localize_script( 'show-courses', 'appLocalizer', [
        'apiUrl' => home_url( '/wp-json' ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );
}


// The custom function MUST be hooked to the init action hook
add_action( 'init', 'sc_register_courses_post_type' );

function sc_register_courses_post_type() {

    $labels = array(
        'name' => _x( 'Courses', 'post type general name' ),
        'singular_name' => _x( 'Course', 'post type singular name' ),
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'menu_icon' => 'dashicons-excerpt-view',
        'capability_type' => 'post',
    );

    register_post_type( 'courses', $args );

    register_taxonomy(
        'coursescat',
        array('courses'),
        array(
            'label' => __( 'Category' ),
            'hierarchical' => true,
            'show_in_rest' => true,
            'publicly_queryable' => false,
            'show_admin_column' => true,
        )
    );
}