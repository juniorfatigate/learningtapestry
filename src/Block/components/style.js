const CoursesBlock = {
    container: {
        width: '80%',
        margin: "auto"
    },

    title: {
        fontFamily: 'Inter',
        fontWeight: '800',
        fontSize: '60px',
        lineHeight: '60px',
        textAlign: 'center',
        letterSpacing: '-0.025em',
        color: '#111827'
    },

    description: {
        fontFamily: 'Inter',
        fontWeight: '400',
        fontSize: '20px',
        lineHeight: '28px',
        textAlign: 'center',
        color: '#6B7280',
        maxWidth: '576px',
        margin: 'auto',
        marginTop: '20px'
    },

    containerCourses: {
        display: 'flex',
        flexDirection: "row",
        marginTop: '96px',
        flexWrap: 'wrap'
    },

    buttonEnable: {
        fontFamily: 'Inter',
        padding: '9px 17px',
        fontWeight: '500',
        fontSize: '16px',
        lineHeight: '24px',
        boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.05)',
        borderRadius: '6px',
        margin: 'auto',
        marginTop: '80px',
        marginBottom: '40px',
        display: 'block',
        color: '#FFFFFF',
        background: '#4F46E5'
    },

    buttonDisabled: {
        fontFamily: 'Inter',
        padding: '9px 17px',
        fontWeight: '500',
        fontSize: '16px',
        lineHeight: '24px',
        boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.05)',
        borderRadius: '6px',
        margin: 'auto',
        marginTop: '80px',
        marginBottom: '40px',
        display: 'block',
        color: '#6B7280',
        background: '#FAFAFA',
    }
}

const CourseStyle = {
    containerCourse: {
        width: '50%',
        borderRadius: '8px 0px 0px 0px',
        padding: '24px',
        boxShadow: '0px 1px 2px #E5E7EB',
    },

    titleCourse: {
        fontFamily: 'Inter',
        fontWeight: '500',
        fontSize: '20px',
        lineHeight: '28px',
        color: '#111827'
    },

    excerptCourse: {
        fontFamily: 'Inter',
        fontWeight: '400',
        fontSize: '14px',
        lineHeight: '20px',
        color: '#6B7280'
    },

    linkCourse: {
        fontFamily: 'Inter',
        padding: '9px 17px',
        border: '1px solid #D1D5DB',
        boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.05)',
        borderRadius: '6px',
        fontWeight: '500',
        fontSize: '14px',
        lineHeight: '20px',
        color: '#374151',
        marginTop: '28px',
        display: 'block',
        width: '115px'
    }
}

export {
    CoursesBlock,
    CourseStyle
}
