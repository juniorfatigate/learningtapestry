import React from 'react';
import { CourseStyle } from "./style";

const Course = (props) => {
    const {course} = props
    return (
        <React.Fragment>
            <div style={CourseStyle.containerCourse}>
                <h2 style={CourseStyle.titleCourse}>{course.title}</h2>
                <div style={CourseStyle.excerptCourse}>{course.excerpt}</div>
                <a style={CourseStyle.linkCourse} href={course.link}>Learn more</a>
            </div>
        </React.Fragment>
    )
}

export default Course