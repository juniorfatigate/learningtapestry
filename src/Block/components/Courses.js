import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Course from "./Course";
import {CoursesBlock} from "./style";

const Courses = () => {
    const [title, setTitle ] = useState('');
    const [description, setDescription ] = useState('');
    const [numberCoursesDisplay, setNumberCoursesDisplay ] = useState(0);
    const [paged, setPaged ] = useState(1);
    const [courses, setCourses ] = useState([]);
    const [loaded, setLoaded ] = useState(false);
    const [moreCourses, setMoreCourses ] = useState(true);
    const [buttonStyle, setButtonStyle ] = useState(CoursesBlock.buttonEnable);

    const url =`${appLocalizer.apiUrl}/sc/v1/settings`
    const urlCourses =`${appLocalizer.apiUrl}/lt/v1/courses`

    const handleClick = () => {
        setLoaded(false)
        axios.get(`${urlCourses}?page=${paged}`)
        .then((response) => {
            if(response.data.length === 0){
                disabledButton()
            } else {
                let joined = courses.concat(response.data);
                setCourses(joined)
                setPaged(paged + 1)
                if(response.data.length < numberCoursesDisplay) {
                    disabledButton()
                }
            }

        })
        setLoaded(true)
    }

    const disabledButton = () => {
        setMoreCourses(false)
        setButtonStyle(CoursesBlock.buttonDisabled)
    }

    useEffect(() => {
        axios.get(url)
        .then((response) => {
            setTitle(response.data.title)
            setDescription(response.data.description)
            setNumberCoursesDisplay(response.data.number_courses_display)
        })
        axios.get(`${urlCourses}?page=${paged}`)
        .then((response) => {
            if(response.data.length === 0){
                disabledButton()
            } else {
                setCourses(response.data)
                setPaged(paged + 1)
                if(response.data.length < numberCoursesDisplay) {
                    console.log('numberCoursesDisplay', numberCoursesDisplay)
                    disabledButton()
                }
            }
        })
        setLoaded(true)
    },[])

    return (
        <React.Fragment>
            <div style={CoursesBlock.container}>
                <article>
                    <h2 style={CoursesBlock.title}>{title}</h2>
                    <p style={CoursesBlock.description}>{description}</p>
                    <div style={CoursesBlock.containerCourses}>
                        {loaded ? courses.map((course, key) => {                    
                                return (<Course key={key} course={course} />) 
                            }) : 'not'}
                    </div>
                    <button style={buttonStyle} type="button" onClick={() => handleClick()} disabled={!moreCourses}>Load more courses</button>
                </article>
            </div>
        </React.Fragment>
    )
}

export default Courses