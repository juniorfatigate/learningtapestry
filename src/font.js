import WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: ['Inter Web:400,500,800', 'sans-serif']
  }
});