import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Settings = () => {

    const [title, setTitle ] = useState('');
    const [description, setDescription ] = useState('');
    const [number_courses_display, setNumberCoursesDisplay ] = useState('');
    const [loader, setLoader ] = useState('Save Settings');

    const url =`${appLocalizer.apiUrl}/sc/v1/settings`

    const handleSubmit = (event) => {
        event.preventDefault()
        setLoader('Saving...')
        axios.post(url,{
                title,
                description,
                number_courses_display
            }, 
            {
                headers: {
                    'content-type': 'application/json',
                    'X-WP-NONCE': appLocalizer.nonce
                }
            }
        )
        .then((response) => {
            setLoader('Save Settings')
            console.log(response)
        })
    }

    useEffect(() => {
        axios.get(url)
        .then((response) => {
            setTitle(response.data.title)
            setDescription(response.data.description)
            setNumberCoursesDisplay(response.data.number_courses_display)
        })
    },[])

    return (
        <React.Fragment>
            <h2>Course Overview</h2>
            <form id="show-course-form" onSubmit={(event) => handleSubmit(event) }>
                <table className="form-table" role="presentation">
                    <tbody>
                        <tr>
                            <th scope="row">
                                <label htmlFor="title">Title</label>
                            </th>
                            <td>
                                <input id="title" name="title" value={title} 
                                onChange={(event) => {setTitle(event.target.value)}} 
                                className="regular-text"/>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label htmlFor="description">Description</label>
                            </th>
                            <td>
                                <input id="description" name="description" value={description} 
                                onChange={(event) => {setDescription(event.target.value)}} 
                                className="regular-text"/>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label htmlFor="number_courses_display">Number of courses to display</label>
                            </th>
                            <td>
                                <input id="number_courses_display" name="number_courses_display" 
                                type="number" value={number_courses_display} 
                                onChange={(event) => {setNumberCoursesDisplay(event.target.value)}} 
                                className="regular-text"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p className="submit">
                    <button type="submit" className="button button-primary">{loader}</button>
                </p>
            </form>
        </React.Fragment>
    )
}

export default Settings